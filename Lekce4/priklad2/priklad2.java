package Lekce4.priklad2;

import java.util.Scanner;

public class priklad2 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Zadejte delší slovo:");
        String prvniSlovo = sc.nextLine();
        System.out.println("Zadejte kratší slovo:");
        String druheSlovo = sc.nextLine();
        int delkaPrvni = prvniSlovo.length();
        int delkaDruhe = druheSlovo.length();
        int porovnani = delkaPrvni - delkaDruhe;
        System.out.printf("Slova se liší délkou o %d " + "znaků", porovnani);

    }
}
