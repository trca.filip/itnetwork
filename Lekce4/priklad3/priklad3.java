package Lekce4.priklad3;

import java.util.Locale;
import java.util.Scanner;

public class priklad3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Zadej řetězec:");
        String retezec = sc.nextLine();
        String retezecLow = retezec.toLowerCase(Locale.ROOT);
        System.out.println(retezecLow.contains("itnetwork"));
    }
}
