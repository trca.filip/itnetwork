package Lekce4.priklad1;

import java.util.Locale;
import java.util.Scanner;

public class priklad1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Zadej jméno:");
        String jmeno = sc.nextLine();
        System.out.println("Zadej příjmení:");
        String prijmeni = sc.nextLine();
        System.out.println("Zadej svůj věk:");
        int vek = Integer.parseInt(String.valueOf(sc.nextInt()));
        int vek2 = vek + 1;
        String jmenoUpper = jmeno.toUpperCase(Locale.ROOT);
        String prijmeniUpper = prijmeni.toUpperCase(Locale.ROOT);
        System.out.println(jmenoUpper + " " + prijmeniUpper);
        System.out.println("Za rok ti bude " + vek2 + " let.");

    }
}
