package Lekce3.priklad3;

import java.util.Scanner;

public class priklad3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Zadej poloměr kruhu (cm):");
        double polomer = Double.parseDouble(sc.next());
        double obvod = 2 * 3.1415 * polomer;
        double obsah = 3.1415 * (polomer*polomer);
        System.out.println("Obvod zadaného kruhu je: " + obvod + " cm");
        System.out.println("Jeho obsah je: "+obsah+ " cm^2");
    }
}
