package Lekce3.priklad1;

import java.util.Scanner;

public class priklad1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ahoj, jak se jmenuješ?");
        String jmeno = sc.nextLine();
        System.out.println("Jaký jsi?");
        String vlastnost = sc.nextLine();

        System.out.println(jmeno + " je " + vlastnost + ".");
    }
}
