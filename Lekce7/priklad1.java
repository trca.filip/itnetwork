package Lekce7;

public class priklad1 {
    public static void main(String[] args) {
        // Inicializace
        int[] cisla = new int[10];
        // Zápis
        for (int i = 0; i < cisla.length; i++) {
            cisla[i] = 10 - i;
        }
        // Výpis
        for (int i : cisla) {
            System.out.println(i);
        }
    }
}
