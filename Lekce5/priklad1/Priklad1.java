package Lekce5.priklad1;

import java.util.Scanner;

public class Priklad1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Zadej své jméno:");
        String jmeno = sc.nextLine();
        int delkaJmeno = jmeno.length();

        if ((delkaJmeno >= 3) && (delkaJmeno <= 10)) {
            System.out.println("Normální jméno.");
        } else {
            System.out.println("Máš moc krátké nebo moc dlouhé jméno!");
        }

    }
}
