package Lekce5.priklad2;

import java.util.Scanner;

public class Priklad2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Zadej smajlíka:");
        String smailik = sc.nextLine();

        switch (smailik) {
            case (":-)") -> System.out.println("Tvůj smajlík je veselý");
            case (":-(") -> System.out.println("Tvůj smajlík je smutný");
            case (":-*") -> System.out.println("Tvůj smajlík je zamilovaný");
            case (":-p") -> System.out.println("Tvůj smajlík je s vyplazeným jazykem");
            default -> System.out.println("Tvůj smajlík je neznámý");
        }
    }
}
